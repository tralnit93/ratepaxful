package com.example.ratepaxful;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Lê Nguyên Trà on 3/26/2020.
 * Copyright © 2020 VNPT IT 3. All rights reserved.
 */
public class App extends Application {
    private static App sInstance = null;

    /**
     * Get instance of app
     *
     * @return app
     */
    public static synchronized App getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;

        // 1 - init realm
        initRealm();


    }

    private void initRealm() {

        Realm.init(this);

        // initAuthSystem config
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfig);
    }
}
