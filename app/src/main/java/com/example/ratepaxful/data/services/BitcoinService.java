package com.example.ratepaxful.data.services;

import com.example.ratepaxful.data.model.CurrentModel;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Lê Nguyên Trà on 3/26/2020.
 * Copyright © 2020 VNPT IT 3. All rights reserved.
 */
public interface BitcoinService {
    @GET("v2/prices/spot")
    Flowable<List<CurrentModel>> getCurretPrice(@Query("currency") String key);
}
