package com.example.ratepaxful.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lê Nguyên Trà on 3/26/2020.
 * Copyright © 2020 VNPT IT 3. All rights reserved.
 */
public class CurrentModel {

    @SerializedName("data")
    @Expose
    private CurrentDetailModel data;

    public CurrentDetailModel getData() {
        return data;
    }

    public void setData(CurrentDetailModel data) {
        this.data = data;
    }

}
