package com.example.ratepaxful.ui;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ratepaxful.R;
import com.example.ratepaxful.data.model.CurrentModel;
import com.example.ratepaxful.data.services.BitcoinService;
import com.example.ratepaxful.data.utils.PrefUtils;
import com.example.ratepaxful.ui.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

import java.util.List;
import java.util.UUID;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_rate_page)
public class RateFragment extends BaseFragment {
    private BitcoinService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setData() {

    }
    private void getPrice() {
        // unique id to identify the device


    }

}
