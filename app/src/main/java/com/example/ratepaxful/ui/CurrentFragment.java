package com.example.ratepaxful.ui;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ratepaxful.R;
import com.example.ratepaxful.ui.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_rate_page)
public class CurrentFragment extends BaseFragment {


    @Override
    protected void setData() {

    }
}
