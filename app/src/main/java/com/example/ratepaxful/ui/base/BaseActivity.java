package com.example.ratepaxful.ui.base;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import java.util.Objects;

import io.realm.Realm;

/**
 * Created by Trần Vũ Hiếu on 29/12/2015.
 * Copyright © 2017 VNPT DANANG. All rights reserved.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity {
    public abstract void onPermissionsGranted(int requestCode);

    protected final String LOG_TAG = this.getClass().getSimpleName();
    public static final String EXTRA_REQUEST_CODE = "REQUEST_CODE";

    private SparseIntArray mErrorString;
    protected Realm mRealm;

//    private AwesomeProgressDialog progressDialog;
//    private MenuChucNang chucNang;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart" + LOG_TAG);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop" + LOG_TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "Resum" + LOG_TAG);
        adjustFontScale(getResources().getConfiguration());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Tú thêm kiểm tra font chữ thiết bị
        adjustFontScale(getResources().getConfiguration());

        Log.d(LOG_TAG, "NameScreen Create: " + LOG_TAG);
        mErrorString = new SparseIntArray();
        if (mRealm == null) {
            // Khởi tạo đối tượng realm.
            mRealm = Realm.getDefaultInstance();
        }

        //get data
//        chucNang = (MenuChucNang) getIntent().getSerializableExtra(EXTRA_CHUCNANG);
//        if (chucNang == null) {
//            chucNang = Injection.provideMenuChucNangRepository().getChucNangWithUrl(this.getClass().getName());
//        }
    }

    // Tú thêm kiểm tra font chữ thiết bị lớn thì chỉnh về kích cỡ bình thường
    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > 1) {
            configuration.fontScale = (float) 1;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            Objects.requireNonNull(this).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);

        }
    }

//    public MenuChucNang getMenuChucNang() {
//        return chucNang;
//    }
//
//    public AwesomeProgressDialog showLoading(String msg) {
//        hideLoading();
//        progressDialog = DialogUtils.showProgressDialog(this, "Đang tải", msg, false);
//        return progressDialog;
//    }
//
//    public void hideLoading() {
//        if (progressDialog != null) {
//            progressDialog.hide();
//        }
//    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);

    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");
        super.onDestroy();
        closeRealm();
    }

    @AfterViews
    protected void onAfterView() {
        Log.d(LOG_TAG, "onAfterView");
        setValue();
        setEvent();

    }

    protected abstract void setValue();

    protected abstract void setEvent();

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int permissionCheck = PackageManager.PERMISSION_GRANTED;

        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }

        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            onPermissionsGranted(requestCode);
        } else {
            Snackbar.make(findViewById(android.R.id.content), mErrorString.get(requestCode),
                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                    v -> {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }).show();
        }
    }

    @SuppressWarnings("unused")
    public void requestAppPermissions(final String[] requestedPermissions, final int stringId, final int requestCode) {
        mErrorString.put(requestCode, stringId);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        boolean shouldShowRequestPermissionRationale = false;

        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(this, permission);
            shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale || ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
        }

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale) {
                Snackbar.make(findViewById(android.R.id.content), stringId,
                        Snackbar.LENGTH_INDEFINITE).setAction("GRANT",
                        v -> ActivityCompat.requestPermissions(BaseActivity.this, requestedPermissions, requestCode)).show();
            } else {
                ActivityCompat.requestPermissions(this, requestedPermissions, requestCode);
            }
        } else {
            onPermissionsGranted(requestCode);
        }
    }

    /**
     * Close database.
     */
    private void closeRealm() {
        if (mRealm != null && !mRealm.isClosed()) {
            try {
                mRealm.close();
            } catch (Exception e) {
                Log.d(LOG_TAG, "Couldn't close realm.");
            }
        }
    }

    public interface IOnBackPressed {
        void onBackPressed();
    }


}
