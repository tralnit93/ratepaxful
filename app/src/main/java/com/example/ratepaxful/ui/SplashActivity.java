package com.example.ratepaxful.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;


import com.example.ratepaxful.BuildConfig;
import com.example.ratepaxful.R;

import org.androidannotations.annotations.EActivity;


@SuppressLint("Registered")
@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set up Crashlytics, disabled for debug builds

        super.onCreate(savedInstanceState);
        new Handler().postDelayed(() -> {
            MainActivity_.intent(SplashActivity.this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
        }, 1000);
    }
}
