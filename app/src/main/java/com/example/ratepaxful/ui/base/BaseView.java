package com.example.ratepaxful.ui.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
