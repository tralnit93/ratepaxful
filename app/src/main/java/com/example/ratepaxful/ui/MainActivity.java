package com.example.ratepaxful.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ratepaxful.R;
import com.example.ratepaxful.common.MainPageType;
import com.example.ratepaxful.ui.base.BaseActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    private static final int MAX_MAIN_PAGE = 2;
    @ViewById
    ViewPager viewPager;
    @ViewById
    BottomNavigationView bottomNavigationViewEx;
    @ViewById
    Toolbar toolbar;
    private List<String> fragmentsTitle;// used for ViewPager adapter
    private SparseIntArray items;// used for change ViewPager selected item

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    @Override
    protected void setValue() {
        setupViewPager();
        setSupportActionBar(toolbar);
    }

    private void setupViewPager() {
        // used for ViewPager adapter
        List<Fragment> fragments = new ArrayList<>(MAX_MAIN_PAGE);
        fragmentsTitle = new ArrayList<>(MAX_MAIN_PAGE);
        items = new SparseIntArray(MAX_MAIN_PAGE);

        // Create Home fragment
        RateFragment rateFragment = RateFragment_.builder().build();
//        new HomePagePresenter(Injection.provideMenuChucNangRepository(), homeFragment);

        // Notification fragment
        CurrentFragment currentFragment = CurrentFragment_.builder().build();
//        new NotificationPagePresenter(Injection.provideThongBaoRepository(),notificationFragment);

        // create Menu fragment and add it


        // add to fragments for adapter
        fragments.add(rateFragment);
        fragments.add(currentFragment);

        fragmentsTitle.add(getResources().getString(R.string.title_fragment_rate));
        fragmentsTitle.add(getResources().getString(R.string.title_fragment_current));

        // add to items for change ViewPager item
        items.put(R.id.item_navigation_rate, MainPageType.RATE_PAGE.getValue());
        items.put(R.id.item_navigation_current, MainPageType.CURRENT_PAGE.getValue());

        //set adapter
        //data
        MainBottomNavigationAdapter adapter = new MainBottomNavigationAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(adapter);
        toolbar.setTitle(fragmentsTitle.get(viewPager.getCurrentItem()));
    }

    @Override
    protected void setEvent() {
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            private int previousPosition = -1;

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int position = items.get(item.getItemId());
                if (previousPosition != position) {
                    // only set item when item changed
                    previousPosition = position;
                    viewPager.setCurrentItem(position, true);
                }
                return true;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
                if (position == 2)
                    Objects.requireNonNull(getSupportActionBar()).hide();
                else
                    Objects.requireNonNull(getSupportActionBar()).show();
                toolbar.setTitle(fragmentsTitle.get(position));
                bottomNavigationViewEx.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_homepage, menu);

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
