package com.example.ratepaxful.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import io.realm.Realm;

/**
 * Created by Trần Vũ Hiếu on 29/12/2015.
 * Copyright © 2017 VNPT DANANG. All rights reserved.
 */
@EFragment
public abstract class BaseFragment extends Fragment {
    protected final String LOG_TAG = this.getClass().getSimpleName();
    protected Realm mRealm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // Khởi tạo đối tượng realm.
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    @AfterViews
    protected void onAfterView() {
        Log.d(LOG_TAG, "onAfterView");
        if (getView() != null) {
            getView().setClickable(true);
        }

        setData();
    }

    protected abstract void setData();

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");
        super.onDestroy();

        closeRealm();
    }

    /**
     * Close realm database.
     */
    private void closeRealm() {
        if (mRealm != null && !mRealm.isClosed()) {
            try {
                mRealm.close();
            } catch (Exception e) {
                Log.d(LOG_TAG, "Couldn't close realm.");
            }

            mRealm = null;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Disable click layout below layout above when replace or add fragment;
        // Hide keyboard when view focus current inside edit text
//        KeyboardUtil.hideKeyboard(getView(), getActivity());
        Log.d(LOG_TAG, "onActivityCreated");
    }
    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(BaseActivity.EXTRA_REQUEST_CODE, requestCode);
        super.startActivityForResult(intent, requestCode);
    }

}
