package com.example.ratepaxful.ui;

import com.example.ratepaxful.data.model.CurrentModel;
import com.example.ratepaxful.ui.base.BasePresenter;
import com.example.ratepaxful.ui.base.BaseView;

import java.util.List;

/**
 * Created by Lê Nguyên Trà on 3/26/2020.
 * Copyright © 2020 VNPT IT 3. All rights reserved.
 */
public interface BitcoinPriceContract {
    interface View extends BaseView<Presenter> {

        boolean isActive();
        void showPriceToUsd(List<CurrentModel> currentModels);
        void showPriceToVnd(List<CurrentModel> currentModels);

        void showLoadingIndicator(boolean active);
    }

    interface Presenter extends BasePresenter {

        void getCurrentPriceBitcoin(String key);

        void dispose();
    }
}
