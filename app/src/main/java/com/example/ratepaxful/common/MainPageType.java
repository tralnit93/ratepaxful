package com.example.ratepaxful.common;

/**
 * Created by Trần Vũ Hiếu on 4/24/18.
 * Copyright © 2018 VNPT DANANG. All rights reserved.
 */
public enum MainPageType {
    RATE_PAGE(0),
    //    SALE_PAGE(1),
//    CART_PAGE(2),
//    NOTIFICATION(3),
//    MENU_PAGE(4);
    CURRENT_PAGE(1);

    private final int value;

    MainPageType(int val) {
        this.value = val;
    }

    public int getValue() {
        return value;
    }
}
